# Test Contact

Formulaire d'enregistrement de contacts avec statistiques sous Laravel 5.7

## Pré-requis

- PHP >= 7.2
- MySQL >= 5.5
- Node.js
- Composer

## Installation

Récupération du projet :
```
git clone git@bitbucket.org:goldenscarab/test-contact.git
```

Installation de Laravel :
```
composer install
```

Initialisation de Laravel:
```
cp .env.example .env
php artisan key:generate
php artisan config:cache
```

Base de données :
```
php artisan migrate --seed
```

Dépendances front :
```
npm install
npm run prod
```


## Configuration

La configuration se fait dans le fichier .env.
Après un changement de configuration du fichier .env: 
```
php artisan config:cache
```

## Administration

## Auteur
Sylvain CARRE
sylvain.carre@goldenscarab.fr

## Documentation
[Arborescence](https://www.gloomaps.com/JT6Rqwazhp)