<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Repositories\ContactRepository;
use App\Http\Requests\ContactStorePost;


class ContactController extends Controller
{
    
    /**
     * Repo de contact
     * @var ContactRepository
     */
    private $contact_repo;


    public function __construct(ContactRepository $contact_repo)
    {
    	$this->contact_repo = $contact_repo;
    }

    /**
     * Création d'un nouveau contact
     * @return View Le formulaire de création
     */
    public function create()
    {
    	// Création d'un nouveau contact
    	$contact = $this->contact_repo->new();

    	return view('pages.contact.form')->with([
			'contact' => $contact
    	]);
    }

    /**
     * Enregistre un contact
     * @param  ContactStorePost $request Les données du formulaire
     * @return Redirect                    Formulaire de création
     */
    public function store(ContactStorePost $request)
    {
        // On récupère les données postées
        $data = collect($request->except(['_token']));

        // On sauvegarde les données
        $contact = $this->contact_repo->store($data, null);

        // Ajout d'un tracking sur le post en base de données
        dispatch(new \App\Jobs\RequestPostTracker($contact));

        // Message de validation pour l'utilisateur
        \Session::flash('success', "Contact ajouté avec succès. Merci pour votre contribution."); 

        // Redirection
        return \Redirect::route('create', app()->getLocale());
    }

    /**
     * Liste des contacts enregistré
     * @param  Request $request La requête demandée
     * @return View           Le HTML de la page list
     */
    public function list(Request $request)
    {
        if(!$request->ajax()){
            return abort(403);
        }

        $contacts = $this->contact_repo->all();

        return view('pages.contact.list')->with([
            'contacts' => $contacts
        ]);
    }

    /**
     * Statistique sur les départements des contacts
     * @param  Request $request La requête demandée
     * @return View           Le HTML de la page stats
     */    
    public function stats(Request $request)
    {
        if(!$request->ajax()){
            return abort(403);
        }

        // On récupère les stats
        $departments = \ContactService::makeStatsByDepartment();

        return view('pages.contact.stats')->with([
            'departments' => $departments
        ]);
    }
}
