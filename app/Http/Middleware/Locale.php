<?php

namespace App\Http\Middleware;

use Closure;

class Locale
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locales_availables = config('app.locales_availables');

        // Si execution console alors on quitte
        if (\App::runningInConsole()) {
            return true;
        }

        // Récupération des segments de l'url
        $segments = $request->segments();

        // On récupère le 1er segment
        $locale = $request->segment(1);

        // Si la langue est la même que celle courante
        if ($locale == \App::getLocale()) {
            
            // Suppression du paramètre de lang de la route
            $request->route()->forgetParameter('lang');

            return $next($request);
        }

        // La langue défini n'est pas une langue valide
        if (!in_array($locale, $locales_availables)) {

            // Récupération de la dernière langue par défaut
            $local_current = \App::getLocale();

            // Une langue invalide à été saisie
            if (preg_match('/^[a-z]{2}$/', $locale)) {
                
                // On supprime la mauvaise langue de l'url
                unset($segments[0]);
            }
       
            // Ajout de la langue courante à l'url
            array_unshift($segments, $local_current);

            // Redirection de l'utilisateur avec la bonne langue
            return redirect()->to(url('/', $segments));
        }
        
        // Mise à jour de la Langue de l'application
        \App::setLocale($locale);

        // Suppression du paramètre de lang de la route
        $request->route()->forgetParameter('lang');


        return $next($request);
    }

    public function terminate($request, $response)
    {
        
    }

}
