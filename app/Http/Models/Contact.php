<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /* Accesseurs */
    public function getGeoipAttribute()
    {
        if (isset($this->attributes['geoip'])) {
            return unserialize($this->attributes['geoip']);
        }
    }

    public function setGeoipAttribute($content)
    {
        $this->attributes['geoip'] = serialize($content);
    }

    
    /* Scopes */
    public function scopeSearch($query, $search)
    {
    	if (is_null($search)) {
    		return $query;
    	}

    	return $query->where('firstname',  'LIKE','%' . $search . '%')
            ->orWhere('lastname', 'LIKE','%' . $search . '%')
            ->orWhere('email', 'LIKE','%' . $search . '%')
            ->orWhere('phone', 'LIKE','%' . $search . '%')
            ->orWhere('address', 'LIKE','%' . $search . '%')
            ->orWhere('zipcode', 'LIKE','%' . $search . '%')
            ->orWhere('city', 'LIKE','%' . $search . '%');
    }
}
