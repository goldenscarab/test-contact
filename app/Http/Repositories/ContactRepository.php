<?php

namespace App\Http\Repositories;

use App\Http\Models\Contact;
use Illuminate\Support\Collection;


class ContactRepository implements RepositoryInterface {

	public function all($columns = array('*'))
	{
		return Contact::select($columns)->get();
	}

	public function list($filter, $sort = 'asc', $per_page = 15, $search = "", $customer_id = null)
	{
		return Contact::search($search)->orderBy($filter, $sort)->paginate($per_page);
	}

	public function new()
	{
		return new Contact;
	}

	public function store(Collection $data, $id = null)
	{
		if (is_null($id)) {
			$contact = new Contact;
		} else {
			$contact = Contact::findOrFail($id);
		}
		
		$contact->civility  = $data->get('civility');
		$contact->firstname = $data->get('firstname');
		$contact->lastname  = $data->get('lastname');
		$contact->email     = $data->get('email');
		$contact->phone     = $data->get('phone');
		$contact->address   = $data->get('address');
		$contact->zipcode   = $data->get('zipcode');
		$contact->city      = $data->get('city');

        $contact->save();

        return $contact;
	}

	public function find($id, $columns = array('*'))
	{
		return Contact::select($columns)->find($id);
	}

	public function findOrFail($id, $columns = array('*'))
	{
		return Contact::select($columns)->findOrFail($id);
	}

	public function findBy($field, $value, $columns = array('*'))
	{
		return Contact::select($columns)->where($field, '=', $value)->first();
	}

	public function findOrFailBy($field, $value, $columns = array('*'))
	{
		return Contact::select($columns)->where($field, '=', $value)->firstOrFail();
	}

	public function findAllBy($field, $value, $columns = array('*'))
	{
		return Contact::select($columns)->where($field, '=', $value)->get();
	}

	public function findWhere($where, $columns = array('*')) 
	{
		return Contact::select($columns)->whereRaw($where)->get();
	}

	public function search($search)
	{
		return Contact::search($search)->get();
	}

	public function destroy($id)
	{
		return Contact::findOrFail($id)->delete();
	}

	public function truncate()
	{
		return Contact::truncate();
	}
}