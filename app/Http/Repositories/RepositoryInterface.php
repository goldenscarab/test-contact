<?php

namespace App\Http\Repositories;

use Illuminate\Support\Collection;

interface RepositoryInterface {

	public function all($columns = array('*'));

	public function list($filter, $sort = 'asc', $per_page = 15, $search = "");

	public function new();

	public function store(Collection $data, $id);

	public function find($id, $columns = array('*'));

	public function findOrFail($id, $columns = array('*'));

	public function findBy($field, $value, $columns = array('*'));

	public function findOrFailBy($field, $value, $columns = array('*'));

	public function findAllBy($field, $value, $columns = array('*'));

	public function findWhere($where, $columns = array('*'));

	public function search($search);

	public function destroy($id);

	public function truncate();
	
}