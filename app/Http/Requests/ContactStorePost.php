<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactStorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'my-name'   => 'antirobot_name',
            'my-time'   => 'required|antirobot_time:5',
            'civility'  => 'required|in:1,2',
            'firstname' => 'required|max:255',
            'lastname'  => 'required|max:255',
            'email'     => 'required|email|max:255',
            'phone'     => 'required|phone',
            'address'   => 'required|max:255',
            'zipcode'   => 'required|digits:5',
            'city'      => 'required|max:255',
            'rgpd'      => 'required|in:1'
        ];
    }

     /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'my-name'   => 'Robot',
            'my-time'   => 'Robot',
            'civility'  => __('contact.form.civility.label'),
            'firstname' => __('contact.form.firstname'),
            'lastname'  => __('contact.form.lastname'),
            'email'     => __('contact.form.email'),
            'phone'     => __('contact.form.phone'),
            'address'   => __('contact.form.address'),
            'zipcode'   => __('contact.form.zipcode'),
            'city'      => __('contact.form.city'),
            'rgpd'      => 'RGPD'
        ];
    }
}
