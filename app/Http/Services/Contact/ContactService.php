<?php

namespace App\Http\Services\Contact;

use Illuminate\Support\Collection;
use App\Http\Repositories\ContactRepository;


class ContactService
{
    

    /**
     * Repo de contact
     * @var ContactRepository
     */
    private $contact_repo;


    public function __construct(ContactRepository $contact)
    {
        $this->contact_repo = $contact;
    }

    /**
     * Statistique sur les département des contacts
     * @return Collection Une collection de département avec leur stats
     */
    public function makeStatsByDepartment()
    {
    	// On récupère tous les contacts
    	$contacts = $this->contact_repo->all();

    	$stats_by_department = [];

    	// Récupération de la liste des code département existant
    	$code_list = $contacts->pluck('zipcode')->map(function ($item, $key) {
		    return substr($item, 0, 2);
		});

    	// Pour chacun des codes on calcul les statistiques
    	foreach ($code_list as $code) {

	        // Nombre total de contacts dans ce département
	        $contacts_of_code = $contacts->filter(function ($contact, $key) use ($contacts, $code) {
				    return substr($contact->zipcode, 0, 2) == $code;
			});

			// Nombre de femme dans ce département
			$nb_of_womens = $contacts_of_code->where('civility', 2)->count();

	        $stats_by_department[$code] = (object) [
	        	'nb_contacts'    => $contacts_of_code->count(),
	        	'percent_womens' => ($nb_of_womens * 100) / $contacts_of_code->count()
	        ];
    	}

    	return collect($stats_by_department);
    }


}
