<?php

namespace App\Http\Services\Contact;

use Illuminate\Support\Facades\Facade;

class ContactServiceFacade extends Facade {


    protected static function getFacadeAccessor() 
    { 
    	return 'ContactService';
    } 

}