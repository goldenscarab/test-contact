<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RequestPostTracker implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Url du web service de Geolocalisation
     * @var string
     *
     * Exemple d'api geoip : 
     * https://freegeoip.net/json/
     * https://ipapi.co/IP/json
     * http://ip-api.com/json/ ***
     */
    private $api_url = "http://ip-api.com/json/";


    private $request;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // On récupère l'ip à tracker
        $ip = \Request::ip();
        
        // On récupère la variable d'environnement serveur
        $server = collect(\Request::server());
        
        // Récupération des données geographique de l'ip
        $geoip  = collect(@json_decode(@file_get_contents($this->api_url . $ip)));

        // Construction des données de log
        $tracker = array(
            'ip'              => $ip,
            'request_uri'     => $server->get('REQUEST_URI'),
            'origin_url'      => $server->get('HTTP_REFERER'),
            'request_method'  => $server->get('REQUEST_METHOD'), 
            'http_user_agent' => $server->get('HTTP_USER_AGENT'),
            'provider'        => $geoip->get('isp'),
            'zip_code'        => $geoip->get('zip'),
            'city'            => $geoip->get('city'),
            'region'          => $geoip->get('regionName'),
            'country_name'    => $geoip->get('country'),
            'latitude'        => $geoip->get('lat'),
            'longitude'       => $geoip->get('lon'),
        );

        // On libère de la mémoire
        unset($ip, $server, $geoip, $user_email);

        // On sauvegarde les données dans la donnée
        $this->request->geoip = $tracker;
        $this->request->save();
    }
}
