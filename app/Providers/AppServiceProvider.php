<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Foundation\AliasLoader;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Déclaration des services
        $this->app->bind('ContactService', 'App\Http\Services\Contact\ContactService');

        $loader = AliasLoader::getInstance();
        $loader->alias('ContactService', 'App\Http\Services\Contact\ContactServiceFacade');
    }
}
