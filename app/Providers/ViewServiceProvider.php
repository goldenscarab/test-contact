<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Si execution console alors on quitte
        if (\App::runningInConsole()) {
            return true;
        }
        

        \Blade::directive('errormessage', function ($expression) {
            return '<?php if ($errors->has('.$expression.')): echo \'<div class="invalid-feedback">\'.$errors->first('.$expression.').\'</div>\'; endif; ?>';
        });

        \Blade::directive('haserror', function ($expression) {
            return '<?= $errors->has('.$expression.') ? " is-invalid" : ""; ?>';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
