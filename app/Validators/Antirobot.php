<?php

	// Extension du service de Validation
	\Validator::extend('antirobot_name', function($attribute, $value, $parameters, $validator) {
		
		// On controle que le champs est bien null
		return  preg_match('/^$/', $value) == 1;
	});


	// Extension du service de Validation
	\Validator::extend('antirobot_time', function($attribute, $value, $parameters, $validator) {


		// Calcul du temps entre la construction du formulaire et du post
		$timestamp_form = decrypt($value);
		$timestamp_post = time();
		
		$delay_limit    = $parameters[0];
		$delay_post     = ($timestamp_post - $timestamp_form);

		// Si le delta temps est inférieur à la limite, alors il s'agit d'un robot
		return $delay_post > $delay_limit;
	});
