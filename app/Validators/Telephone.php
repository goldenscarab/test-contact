<?php

	// Extension du service de Validation
	\Validator::extend('phone', function($attribute, $value, $parameters, $validator) {
		
		return strlen(preg_replace('#^.*([0-9]{3})[^0-9]*([0-9]{3})[^0-9]*([0-9]{4})$#', '$1$2$3', $value)) == 10;
	});

	// Remplacement du message d'erreur
	// \Validator::replacer('phone', function($message, $attribute, $rule, $parameters) {

	// 	return 'Le numéro de téléphone saisie n\'est pas valide';
	// });
