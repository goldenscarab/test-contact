
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(document).ready(function () {

    var datatable = null;
    
    // Au clic sur le boutons « Contacts »
    $('#contact-list button').on('click', function() {

    	// On fait le ménage avec la mise à jour
    	if (datatable != null) {
    		datatable.destroy();
    	}

    	$.ajax(
		{
			url      :  '/' + locale + '/contacts',
			type     : 'GET',
			dataType : 'html',
			async    : true,

			success  : function(html){ 
				
				// Mise à jour de la section liste des contacts
				$('#contact-list .content').html(html);

				// On applique le plugin datatable pour les tris
				datatable = $('table#contact-list').DataTable();

				// On positionne le scroll sur le bloc qui nous interresse
				window.location.hash = "#contact-list";
				
				// Une fois en place on va forcer l'affichage 40 pixels plus haut.
				$(window).scrollTop( $(window).scrollTop() - 40 );

			},
			error    : function(result){ 
				alert('Erreur de communication...') 
			},
		});

    });


     // Au clic sur le boutons « Contacts »
    $('#statistics button').on('click', function() {

	   	$.ajax(
		{
			url      :  '/' + locale + '/statistiques',
			type     : 'GET',
			dataType : 'html',
			async    : true,

			success  : function(html){ 
				
				// Mise à jour de la section liste des stats
				$('#statistics .content').html(html);

				// On positionne le scroll sur le bloc qui nous interresse
				window.location.hash = "#statistics";
				
				// Une fois en place on va forcer l'affichage 40 pixels plus haut.
				$(window).scrollTop( $(window).scrollTop() - 40 );

			},
			error    : function(result){ 
				alert('Erreur de communication...') 
			},
		});

    });
} );

