<?php

return [
	'form.title'             => "Creating a new contact",
	'form.subtitle'          => "Please fill the form below to add you to the list of contacts",
	'form.civility.label'    => "Civility",
	'form.civility.choice'   => "Choose an option...",
	'form.civility.madame'   => "Madame",
	'form.civility.monsieur' => "Monsieur",
	'form.firstname'         => "Firstname",
	'form.lastname'          => "Lastname",
	'form.email'             => "E-mail",
	'form.phone'             => "Phone",
	'form.address'           => "Address",
	'form.zipcode'           => "Zipcode",
	'form.city'              => "City",
	'form.rgpd'              => "By submitting this form, I agree that my data will be released publicly for statistical purposes.",
	'form.save'              => "Save",

	'list.title'             => "List of registered contacts",
	'list.subtitle'          => "For reasons of discretion, not all information is displayed here.",
	'list.empty'             => "No contacts were found",

	'stats.title'            => "Registered contact statistics",
	'stats.button'           => "Statistics",
	'stats.code'             => "Department number",
	'stats.contacts'         => "Number of contacts in this department",
	'stats.womens'           => "Percentage of women in this department",
	'stats.empty'            => "No statistics available"
];
