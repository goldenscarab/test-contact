<?php

return [

	'form.title'             => "Création d'un nouveau contact",
	'form.subtitle'          => "Veuillez remplir le formulaire ci-dessous pour vous rajouter à la liste des contacts",
	'form.civility.label'    => "Civilité",
	'form.civility.choice'   => "Choisir une option...",
	'form.civility.madame'   => "Madame",
	'form.civility.monsieur' => "Monsieur",
	'form.firstname'         => "Prénom",
	'form.lastname'          => "Nom",
	'form.email'             => "Courriel",
	'form.phone'             => "Téléphone",
	'form.address'           => "Adresse",
	'form.zipcode'           => "Code postal",
	'form.city'              => "Ville",
	'form.rgpd'              => "En soumettant ce formulaire, j'accepte que mes données soit diffusées de façon public à des fins de statistiques.",
	'form.save'              => "Enregister",

	'list.title'             => "Liste des contacts inscrit",
	'list.subtitle'          => "Pour des raisons de discrétion, toutes les informations ne sont pas affichées ici.",
	'list.empty'             => "Aucun contact n'a été trouvé",

	'stats.title'            => "Statistiques des contacts inscrits",
	'stats.button'           => "Statistiques",
	'stats.code'             => "Numéro département",
	'stats.contacts'         => "Nombre de contacts dans ce département",
	'stats.womens'           => "Pourcentage de femmes dans ce département",
	'stats.empty'            => "Aucune statistiques disponibles"
];
