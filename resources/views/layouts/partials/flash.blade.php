<!-- Messsage Flash -->
@if(Session::has('success'))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<i class="fa fa-check mr-1" aria-hidden="true"></i>
		{!! Session::get('success') !!}
	</div>
@endif
@if(Session::has('danger'))
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<i class="fa fa-times mr-1" aria-hidden="true"></i>
		{!! Session::get('danger') !!}
	</div>
@endif
@if(Session::has('error'))
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<i class="fa fa-times mr-1" aria-hidden="true"></i>
		{!! Session::get('error') !!}
	</div>
@endif
@if(Session::has('info'))
	<div class="alert alert-info alert-dismissible fade show" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<i class="fa fa-info-circle mr-1" aria-hidden="true"></i>
		{!! Session::get('info') !!}
	</div>
@endif
@if(Session::has('warning'))
	<div class="alert alert-warning alert-dismissible fade show" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<i class="fa fa-exclamation-triangle mr-1" aria-hidden="true"></i>
		{!! Session::get('warning') !!}
	</div>
@endif