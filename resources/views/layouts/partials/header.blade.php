<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">Contact</h5>
    <nav class="my-2 my-md-0 mr-md-3">
{{--         <a class="p-2{{ Request::is('*/creer') ? '' : ' text-dark'}}" href="{{ route('create', app()->getLocale()) }}">
            {{ __('menu.add') }}
        </a>
        <a class="p-2{{ Request::is('*/contacts') ? '' : ' text-dark'}}" href="{{ route('list', app()->getLocale()) }}">
            {{ __('menu.list') }}
        </a>
        <a class="p-2{{ Request::is('*/statistiques') ? '' : ' text-dark'}}" href="{{ route('stats', app()->getLocale()) }}">
            {{ __('menu.stats') }}
        </a> --}}
    </nav>
    @if (app()->getLocale() == 'fr')
        <a class="btn btn-outline-primary" href="{{ route(Route::currentRouteName(), 'en') }}#">
            <img class="ico" src="{{ asset('images/locales/en.png') }}" alt="Flag english"> 
            English
        </a>
    @else
        <a class="btn btn-outline-primary" href="{{ route(Route::currentRouteName(), 'fr') }}">
            <img class="ico" src="{{ asset('images/locales/fr.png') }}" alt="Drapeau français">
            Français
        </a>
    @endif
</div>