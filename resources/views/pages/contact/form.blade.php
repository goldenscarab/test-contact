@extends('layouts.default')


@section('title', "Contact")

@section('content')
	<div id="main-block" class="container">
		
		{{-- Formulaire --}}
		<div class="mb-5">
			<div class="py-5 text-center">
				<h1>{{ __('contact.form.title') }}</h1>
				<p class="lead">{{ __('contact.form.subtitle') }}</p>
			</div>

			<form action="{{ route('store', app()->getLocale()) }}" method="post">
				
				{{ csrf_field() }}

				{{-- Pot de miel pour les robots --}}
			    <div class="d-none">
			        <input type="text" value="" name="my-name">
			        <input type="text" value="{{ encrypt(time()) }}" name="my-time">
			    </div>
				<div class="form-group {{ $errors->has('my-name') || $errors->has('my-time') ? 'was-validated' : '' }}">
				    @if ($errors->has('my-name'))
				    	<small class="form-text text-danger">{!! $errors->first('my-name') !!}</small>
				    @endif
				    @if ($errors->has('my-time'))
				    	<small class="form-text text-danger">{!! $errors->first('my-time') !!}</small>
				    @endif
				</div>

				{{-- Le formulaire pour les humains --}}
				<div class="form-row">
					<div class="form-group col-md-3">
						<label for="exampleInputEmail1">{{ __('contact.form.civility.label') }} *</label>
						<select id="civility" class="custom-select @haserror('civility')" name="civility">
							<option value="" disabled selected hidden>{{ __('contact.form.civility.choice') }}</option>
							<option value="1" {{ old('civility') == 1 ? 'selected' : '' }}>{{ __('contact.form.civility.monsieur') }}</option>
							<option value="2" {{ old('civility') == 2 ? 'selected' : '' }}>{{ __('contact.form.civility.madame') }}</option>
						</select>
						@errormessage('civility')
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="lastname">{{ __('contact.form.lastname') }} *</label>
						<input id="lastname)" type="text" class="form-control @haserror('lastname')" name="lastname" value="{{ old('lastname') }}" placeholder="ex : DOE">
						@errormessage('lastname')
					</div>
					<div class="form-group col-md-6">
						<label for="firstname">{{ __('contact.form.firstname') }} *</label>
						<input id="firstname" type="text" class="form-control @haserror('firstname')" name="firstname" value="{{ old('firstname') }}" placeholder="ex : John">
						@errormessage('firstname')
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="email">{{ __('contact.form.email') }} *</label>
						<div class="input-group mb-2 mr-sm-2">
						    <div class="input-group-prepend">
						      	<div class="input-group-text">@</div>
						    </div>
						    <input id="email" type="email" class="form-control @haserror('email')" id="email" name="email" value="{{ old('email') }}" placeholder="ex : user@mail.tld">
							@errormessage('email')
						</div>
					</div>
					<div class="form-group col-md-6">
						<label for="phone">{{ __('contact.form.phone') }} *</label>
						<div class="input-group mb-2 mr-sm-2">
						    <div class="input-group-prepend">
						      	<div class="input-group-text"><i class="fa fa-phone"></i></div>
						    </div>
						    <input id="phone" type="text" class="form-control @haserror('phone')" id="phone" name="phone" value="{{ old('phone') }}" placeholder=" ex : 0605040302">
							@errormessage('phone')
						</div>
					</div>
				</div>

				<div class="form-group">
				    <label for="address">{{ __('contact.form.address') }} *</label>
				    <input type="text" class="form-control @haserror('address')" id="address" name="address" value="{{ old('address') }}" placeholder="ex : Av. Gambetta">
				    @errormessage('address')
				</div>

				<div class="form-row">
					<div class="form-group col-md-2">
						<label for="zipcode">{{ __('contact.form.zipcode') }} *</label>
						<input id="zipcode" type="zipcode" class="form-control @haserror('zipcode')" id="zipcode" name="zipcode" value="{{ old('zipcode') }}" placeholder="ex : 00000">
						@errormessage('zipcode')
					</div>
					<div class="form-group col-md-10">
						<label for="city">{{ __('contact.form.city') }} *</label>
						<input id="city" type="text" class="form-control @haserror('city')" id="city" name="city" value="{{ old('city') }}" placeholder="ex : TOULOUSE">
						@errormessage('city')
					</div>
				</div>

				<div class="form-check mb-4">
					<input id="rgpd" type="checkbox" class="form-check-input  @haserror('rgpd')" id="rgpd" name="rgpd" value="1">
					<label class="form-check-label" for="rgpd">{{ __('contact.form.rgpd') }}</label>
					@errormessage('rgpd')
				</div>

				<div class="mb-5 text-right">
					<button type="submit" class="btn btn-primary">{{ __('contact.form.save') }}</button>
				</div>
			</form>
					
		</div>
		<hr>
		{{-- Liste des contacts --}}
		<div id="contact-list" class="mb-5">
			<div class="action text-center">
				<button type="button" class="btn btn-outline-info btn-block">Contacts</button>
			</div>
			<div class="content"></div>
		</div>

		<hr>
		{{-- Statistique --}}
		<div id="statistics" class="mb-5">
			<div class="action text-center">
				<button type="button" class="btn btn-outline-info btn-block">{{ __('contact.stats.button') }}</button>
			</div>
			<div class="content"></div>
		</div>
	</div>
@endsection