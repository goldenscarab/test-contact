<div class="container mb-5">
	
	<div class="py-5 text-center">
		<h1>{{ __('contact.list.title') }}</h1>
		<p class="lead">{{ __('contact.list.subtitle') }}</p>
	</div>


	<table id="contact-list" class="table table-hover">
		<thead>
		    <tr>
		      	<th scope="col">#</th>
		      	<th scope="col">{{ __('contact.form.civility.label') }} {{-- col_sort('civility') --}}</th>
		      	<th scope="col">{{ __('contact.form.firstname') }}</th>
		      	<th scope="col">{{ __('contact.form.lastname') }}</th>
		      	{{-- <th scope="col">{{ __('contact.form.email') }}</th> --}}
		      	{{-- <th scope="col">{{ __('contact.form.phone') }}</th> --}}
		      	{{-- <th scope="col">{{ __('contact.form.address') }}</th> --}}
		      	<th scope="col">{{ __('contact.form.zipcode') }}</th>
		      	<th scope="col">{{ __('contact.form.firstname') }}</th>
		      	<th scope="col">Date</th>
		      	<th scope="col"></th>
		    </tr>
		</thead>
		<tbody>
		    @forelse ($contacts as $contact)
			    <tr>
				    <th scope="row">{{ $contact->id }}</th>
				    <td>{{ $contact->civility == 1 ? __('contact.form.civility.monsieur') : __('contact.form.civility.madame') }}</td>
				    <td>{{ $contact->firstname }}</td>
				    <td>{{ $contact->lastname }}</td>
				    {{-- <td>{{ $contact->email }}</td> --}}
				    {{-- <td>{{ $contact->phone }}</td> --}}
				    {{-- <td>{{ $contact->address }}</td> --}}
				    <td>{{ $contact->zipcode }}</td>
				    <td>{{ $contact->city }}</td>
				    <td>{{ $contact->created_at->diffForHumans() }}</td>
			      <td></td>
			    </tr>
		    @empty
				<tr>
					<td colspan="4">{{ __('contact.list.empty') }}</td>
				</tr>
		    @endforelse
	  	</tbody>
	</table>


	<div class="mb-2">
	    <span class="float-right text-muted">{{ $contacts->count() }} {{ str_plural('Contact', $contacts->count()) }}</span>
	</div>
</div>
