<div class="container mb-5">
	
	<div class="py-5 text-center">
		<h1>{{ __('contact.stats.title') }}</h1>
	</div>

	<table class="table table-hover">
		<thead>
			<tr>
				<th>{{ __('contact.stats.code') }}</th>
				<th>{{ __('contact.stats.contacts') }}</th>
				<th>{{ __('contact.stats.womens') }}</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($departments as $code => $stat)
				<tr>
					<td>{{ $code }}</td>
					<td>{{ $stat->nb_contacts }}</td>
					<td>{{ $stat->percent_womens }}</td>
				</tr>
			@empty
				<tr>
					<td colspan="3">{{ __('contact.stats.empty') }}</td>
				</tr>
			@endforelse
		</tbody>
	</table>
</div>
