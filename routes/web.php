<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Home
Route::get('/', function () {

	// Redirection vers la création d'un nouveau contact
	return redirect()->route('create', \App::getLocale());

})->name('index');

// Contacts
Route::middleware(['locale'])->prefix('{lang}')->group(function () {
	Route::get('contacts', 'ContactController@list')->name('list');
	Route::get('creer', 'ContactController@create')->name('create');
	//Route::get('voir/{id}', 'ContactController@read')->name('read');
	//Route::get('modifier/{id}', 'ContactController@update')->name('update');
	Route::post('enregistrer/{id?}', 'ContactController@store')->name('store');
	//Route::get('supprimer/{id}', 'ContactController@delete')->name('delete');

	// Statistiques sur les contacts
	Route::get('statistiques', 'ContactController@stats')->name('stats');
});
